#if SNIPPET1

#include <gsl/gsl>
#include <iostream>

int main(void)
{
	gsl::cstring_span<> str = gsl::ensure_z("Hello World\n");
	std::cout << str.data();

	for (const auto &elem: str)
	{
		std::clog << elem;
	}
}

#endif

#if SNIPPET2

#define GSL_THROW_ON_CONTRACT_VIOLATION
#include <gsl/gsl>
#include <iostream>

int main(void)
{
	try {
		Expects(false);
	}
	catch(const std::exception &e)
	{
		std::cout << "exception: " << e.what() << '\n';
	}
}

#endif

#if SNIPPET3

#define GSL_THROW_ON_CONTRACT_VIOLATION
#include <gsl/gsl>
#include <iostream>

int
test(int i)
{
	Expects(i >= 0 && i < 41);
	i++;

	Ensures(i < 42);
	return i;
}

int main(void)
{
	test(0);

	try {
		test(42);
	}
	catch(const std::exception &e) {
		std::cout << "exception: " << e.what() << '\n';
	}
}

#endif

#if SNIPPET4

#define concat1(a, b) a ## b
#define concat2(a, b) concat1(a,b)
#define ____ concat2(dont_care, __COUNTER__)

#include <gsl/gsl>
#include <iostream>

int main(void)
{
	auto ____ = gsl::finally([]{
		std::cout << "Hello World\n";
	});
}

#endif

#if SNIPPET5

#include <gsl/gsl>
#include <iostream>

int main(void)
{
	uint64_t val = 42;
	
	auto val1 = gsl::narrow<uint32_t>(val);
	auto val2 = gsl::narrow_cast<uint32_t>(val);
}

#endif

#if SNIPPET6

#define GSL_THROW_ON_CONTRACT_VIOLATION
#include <gsl/gsl>
#include <iostream>

int main(void)
{
	uint64_t val = 0xFFFFFFFFFFFFFFFF;
	try {
		gsl::narrow<uint32_t>(val);
	}
	catch(...) {
		std::cout << "narrow failed\n";
	}
}

#endif